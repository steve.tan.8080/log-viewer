﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogViewerApp
{
    public static class Extensions
    {
        public static StringBuilder AppendLineSpecial(this StringBuilder builder, string str)
        {
            var s = str.Replace(@"\r\n", Environment.NewLine);
            return builder.Append(s).Append(Environment.NewLine);
        }
    }
}
