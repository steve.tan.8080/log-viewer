﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using LogViewerApp.Core;

//need error handling, form diaglog popup
//need filter info, error, info etc.

namespace LogViewerApp
{
    static class Program
    {
        static void ConfigureServices(ServiceCollection services) 
        {
            services.AddScoped<FormMain>();
            services.AddTransient<IAppSettingProvider, AppSettingProvider>();
            services.AddTransient<IScintillaConfiguration, ScintillaConfiguration>();
            services.AddTransient<ILogFilter, LogFilter>();
        }

        static void ExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            ErrorManager.Write($"{e.Message}{Environment.NewLine}{e.StackTrace}");
        }

        static void ThreadExceptionHandler(object sender, System.Threading.ThreadExceptionEventArgs arg)
        {
            var e = (Exception)arg.Exception;
            ErrorManager.Write($"{e.Message}{Environment.NewLine}{e.StackTrace}");
        }
        
        [STAThread]
        static void Main()
        {
            //default
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //exceptions
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(ThreadExceptionHandler);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(ExceptionHandler);

            //DI setup
            var services = new ServiceCollection();
            ConfigureServices(services);
            using (ServiceProvider serviceProvider = services.BuildServiceProvider())
            {
                var form = serviceProvider.GetRequiredService<FormMain>();
                Application.Run(form);
            }
        }
    }
}
