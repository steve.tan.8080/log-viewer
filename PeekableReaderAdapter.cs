﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogViewerApp
{
    public class PeekableReaderAdapter
    {
        private StreamReader Underlying { get; }
        private string BufferString { get; set; }

        public PeekableReaderAdapter(StreamReader underlying)
        {
            Underlying = underlying;
        }

        //you can only peek one line
        public string PeekLine()
        {
            if (BufferString == null)
            {
                BufferString = Underlying.ReadLine();
            }

            return BufferString;
        }

        public string ReadLine()
        {
            if (BufferString != null) {
                var str = BufferString;
                BufferString = null;
                return str;
            }            

            return Underlying.ReadLine();
        }

        public bool EndOfStream =>Underlying.EndOfStream && BufferString == null;
        
    }
}
