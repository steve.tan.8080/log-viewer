﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogViewerApp
{
    public class AppSettings
    {
        public string LogFolder { get; set; } = null;
    }

    public interface IAppSettingProvider
    {
        AppSettings Settings { get; set; }
    }

    public class AppSettingProvider: IAppSettingProvider
    {
        public AppSettings Settings { get; set; }

        public AppSettingProvider() 
        {
            var st = File.ReadAllText("./AppSettings.json");
            Settings = JsonConvert.DeserializeObject<AppSettings>(st);
        }
    }
}
