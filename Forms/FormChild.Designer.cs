﻿namespace LogViewerApp
{
    partial class FormChild
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormChild));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.filter_error = new System.Windows.Forms.ToolStripButton();
            this.filter_info = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer2 = new System.Windows.Forms.ToolStripContainer();
            this.panel_text = new System.Windows.Forms.Panel();
            this.refresh = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer2.ContentPanel.SuspendLayout();
            this.toolStripContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filter_error,
            this.filter_info,
            this.refresh});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(2408, 61);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // filter_error
            // 
            this.filter_error.Checked = true;
            this.filter_error.CheckOnClick = true;
            this.filter_error.CheckState = System.Windows.Forms.CheckState.Checked;
            this.filter_error.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.filter_error.Image = ((System.Drawing.Image)(resources.GetObject("filter_error.Image")));
            this.filter_error.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.filter_error.Name = "filter_error";
            this.filter_error.Size = new System.Drawing.Size(103, 52);
            this.filter_error.Text = "error";
            // 
            // filter_info
            // 
            this.filter_info.Checked = true;
            this.filter_info.CheckOnClick = true;
            this.filter_info.CheckState = System.Windows.Forms.CheckState.Checked;
            this.filter_info.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.filter_info.Image = ((System.Drawing.Image)(resources.GetObject("filter_info.Image")));
            this.filter_info.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.filter_info.Name = "filter_info";
            this.filter_info.Size = new System.Drawing.Size(85, 52);
            this.filter_info.Text = "info";
            // 
            // toolStripContainer2
            // 
            this.toolStripContainer2.BottomToolStripPanelVisible = false;
            // 
            // toolStripContainer2.ContentPanel
            // 
            this.toolStripContainer2.ContentPanel.Controls.Add(this.panel_text);
            this.toolStripContainer2.ContentPanel.Controls.Add(this.toolStrip1);
            this.toolStripContainer2.ContentPanel.Size = new System.Drawing.Size(2408, 1284);
            this.toolStripContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer2.LeftToolStripPanelVisible = false;
            this.toolStripContainer2.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer2.Name = "toolStripContainer2";
            this.toolStripContainer2.RightToolStripPanelVisible = false;
            this.toolStripContainer2.Size = new System.Drawing.Size(2408, 1309);
            this.toolStripContainer2.TabIndex = 6;
            this.toolStripContainer2.Text = "toolStripContainer2";
            // 
            // panel_text
            // 
            this.panel_text.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_text.Location = new System.Drawing.Point(0, 61);
            this.panel_text.Name = "panel_text";
            this.panel_text.Size = new System.Drawing.Size(2408, 1223);
            this.panel_text.TabIndex = 6;
            // 
            // refresh
            // 
            this.refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.refresh.Image = ((System.Drawing.Image)(resources.GetObject("refresh.Image")));
            this.refresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(134, 52);
            this.refresh.Text = "refresh";
            // 
            // FormChild
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(19F, 37F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2408, 1309);
            this.Controls.Add(this.toolStripContainer2);
            this.Name = "FormChild";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormChild";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer2.ContentPanel.ResumeLayout(false);
            this.toolStripContainer2.ContentPanel.PerformLayout();
            this.toolStripContainer2.ResumeLayout(false);
            this.toolStripContainer2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton filter_error;
        private System.Windows.Forms.ToolStripButton filter_info;
        private System.Windows.Forms.ToolStripContainer toolStripContainer2;
        private System.Windows.Forms.Panel panel_text;
        private System.Windows.Forms.ToolStripButton refresh;
    }
}