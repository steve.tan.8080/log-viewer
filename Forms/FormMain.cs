﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogViewerApp
{
    public partial class FormMain : Form
    {
        public FormMain(IAppSettingProvider settings, IScintillaConfiguration scintillaConfiguration, ILogFilter logFilter){
            InitializeComponent();
            Settings = settings;
            ScintillaConfiguration = scintillaConfiguration;
            LogFilter = logFilter;
            this.button_open.Click += Button_open_Click;
            this.button_latest.Click += Button_latest_Click;
        }
                
        private void Button_open_Click(object sender, EventArgs e)
        {
            OpenChildForm(OpenFileDialog());
        }
        private void Button_latest_Click(object sender, EventArgs e)
        {
            OpenChildForm(GetLatestLogFile());
        }

        private void OpenChildForm(string path) 
        {
            var f = new FormChild(ScintillaConfiguration, LogFilter);
            f.Text = path;
            f.MdiParent = this;
            f.SetLogBlocks();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
                
        private string GetLatestLogFile() {
            var directory = new DirectoryInfo(this.Settings.Settings.LogFolder);
            var myFile = directory.GetFiles().OrderByDescending(f => f.LastWriteTime).First();
            return myFile.FullName;
        }

        private string OpenFileDialog()
        {
            this.file_open_dialog.InitialDirectory =
                Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            //this.file_open_dialog.Filter = "JS Files (*.js)|*.js|All Files (*.*)|*.*";

            //minimize window, can't hide
            if (this.MdiParent != null && this.MdiParent.ActiveMdiChild != null)
            {
                this.file_open_dialog.InitialDirectory =
                    Path.GetDirectoryName(this.MdiParent.ActiveMdiChild.Text);
            }
            this.WindowState = FormWindowState.Minimized;
            if (this.file_open_dialog.ShowDialog(this) != DialogResult.OK)
            {
                return String.Empty;
            }
            return this.file_open_dialog.FileName;
        }
                
        private IAppSettingProvider Settings { get; }
        private IScintillaConfiguration ScintillaConfiguration { get; }
        private ILogFilter LogFilter { get; }
    }
}
