﻿namespace LogViewerApp
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.button_open = new System.Windows.Forms.ToolStripButton();
            this.button_latest = new System.Windows.Forms.ToolStripButton();
            this.file_open_dialog = new System.Windows.Forms.OpenFileDialog();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.button_open,
            this.button_latest});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1913, 61);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // button_open
            // 
            this.button_open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.button_open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.button_open.Name = "button_open";
            this.button_open.Size = new System.Drawing.Size(111, 52);
            this.button_open.Text = "Open";
            // 
            // button_latest
            // 
            this.button_latest.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.button_latest.Image = ((System.Drawing.Image)(resources.GetObject("button_latest.Image")));
            this.button_latest.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.button_latest.Name = "button_latest";
            this.button_latest.Size = new System.Drawing.Size(117, 52);
            this.button_latest.Text = "Latest";
            // 
            // file_open_dialog
            // 
            this.file_open_dialog.FileName = "log file";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(19F, 37F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1913, 968);
            this.Controls.Add(this.toolStrip1);
            this.IsMdiContainer = true;
            this.Name = "FormMain";
            this.Text = "Form1";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton button_open;
        private System.Windows.Forms.OpenFileDialog file_open_dialog;
        private System.Windows.Forms.ToolStripButton button_latest;
    }
}

