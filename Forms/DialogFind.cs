﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogViewerApp.Forms
{
    public partial class DialogFind : Form
    {
        private Action<string> CallBack { get; }
        public DialogFind(Action<string> callback)
        {
            InitializeComponent();
            CallBack = callback;
            Action<object, KeyEventArgs> act = (sender, e) => 
            {
                //to stop beeping sound when enter key is hit
                e.Handled = true;
                e.SuppressKeyPress = true;

                ExecCallBack();
            };

            HotKeyManager.AddHotKey(this, act, Keys.Enter);
        }

        private void ExecCallBack()
        {
            if (CallBack == null) 
            {
                return;
            }
            CallBack(this.textbox_find.Text);
        }
    }
}
