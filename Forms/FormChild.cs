﻿using LogViewerApp.Forms;
using ScintillaNET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogViewerApp
{
    public partial class FormChild : Form
    {
        public FormChild(IScintillaConfiguration scintillaConfiguration, ILogFilter logFilter)
        {
            InitializeComponent();
            ScintillaConfiguration = scintillaConfiguration;
            LogFilter = logFilter;
            scintillaConfiguration.Load(panel_text);

            this.filter_info.Click += FilterClick;
            this.filter_error.Click += FilterClick;
            this.refresh.Click += RefreshLog;

            this.FindScintilla().KeyPress += FormChild_KeyPress;

            Action<object, KeyEventArgs> actionOpenDialog = (sender, e) =>
            {
                var d = new DialogFind(FindTextCallback);
                d.TopMost = true;
                d.Show();
            };

            HotKeyManager.AddHotKey(this, actionOpenDialog, Keys.F, true, false, false);
        }

        private void FormChild_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 32)
            {
                // Prevent control characters from getting inserted into the text buffer
                e.Handled = true;
                return;
            }
        }

        private void FindTextCallback(string searchText) 
        {
            SearchManager.TextArea = FindScintilla();
            SearchManager.FindFromCursor(searchText);
        }

        private void RefreshLog(object sender, EventArgs e) {
            SetLogBlocks();
        }
        private void FilterClick(object sender, EventArgs e)
        {
            WriteText();
        }

        private void WriteText() {
            var blockTypes = new List<BlockType>();
            
            if (this.filter_error.Checked) { 
                blockTypes.Add(BlockType.Error); 
                blockTypes.Add(BlockType.PS_Error); 
            }
            if (this.filter_info.Checked) { 
                blockTypes.Add(BlockType.Information); 
            }

            if (blockTypes.Count == 0) {
                blockTypes.Add(BlockType.NA);
            }

            var str = LogBlocks.Where(x => blockTypes.Contains(x.BlockType == null ? BlockType.NA : (BlockType)x.BlockType))
                .Aggregate(new StringBuilder(), (sbz, x) => sbz.AppendLineSpecial(x.Block.ToString())).ToString();

            FindScintilla().Text = str;
        }

        private Scintilla FindScintilla() 
        {
            foreach (var control in this.panel_text.Controls)
            {
                if (control.GetType().Name == "Scintilla")
                {
                    return (Scintilla)control;
                }
            }
            return null;
        }

        public void SetLogBlocks() {
            LogBlocks = LogFilter.FilterFile(this.Text, new List<BlockType> { BlockType.Error, BlockType.Information, BlockType.NA });
            WriteText();
        }

        private IScintillaConfiguration ScintillaConfiguration { get; }
        private ILogFilter LogFilter { get; }
        private List<LogBlock> LogBlocks { get; set; }

    }
}
