Log Viewer
==========

Reads from log file, parses content, reverses log items (most recent on top), and writes to output with highlighting.  
 
Tech:
C#, Scintella.Net.

Sample: 
Log file in folder "sample".

Features:
1) loads latest file from preset location.
2) filters items (info, error, warning).
3) refreshes log file in view.

<img src="sample/sample.log.png"></image>
