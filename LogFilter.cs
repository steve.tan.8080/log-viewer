﻿using LogViewerApp.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogViewerApp
{
    public enum BlockType
    {
        Error=10, Information=20, Debug=30, Warning=40, NA = 0,
        PS_Error=100
    }
    public class LogBlock
    {
        public StringBuilder Block { get; set; }
        public BlockType? BlockType { get; set; }

        public LogBlock()
        {
            Block = new StringBuilder();
        }

        public static BlockType? GetBlockType(string line)
        {

            if (line.Contains(" [Error] "))
            {
                return LogViewerApp.BlockType.Error;
            }
            else if (line.Contains(" [Debug] "))
            {
                return LogViewerApp.BlockType.Debug;
            }
            else if (line.Contains(" [Information] "))
            {
                return LogViewerApp.BlockType.Information;
            }
            else if (line.Contains(" [Warning] "))
            {
                return LogViewerApp.BlockType.Warning;
            }
            else if (line.Contains(" [ERR]")) {
                return LogViewerApp.BlockType.PS_Error;
            }


            return null;
        }
    }
    public interface ILogFilter {
        List<LogBlock> FilterFile(string filePath, List<BlockType> blockTypes);
    }
    
    public class LogFilter: ILogFilter
    {
        private LogBlock GetNewBlock(PeekableReaderAdapter reader)
        {
            void setHeader(BlockType blockType, StringBuilder sb, String headerLine){
                //set header
                for (var i = 0; i < 10; i++)
                {
                    //ps error, make regular error, so output can be colorized
                    var zz = blockType == BlockType.PS_Error ? BlockType.Error : blockType;
                    sb.Append($"{zz}---------");
                }
                sb.AppendLine().AppendLine(headerLine);
            }

            StringBuilder FormatBlock(StringBuilder sb)
            {
                //combine 2 new lines into one
                var sb_ = new StringBuilder();
                var arr = sb.ToString().Split(new String[] { Environment.NewLine + Environment.NewLine }, StringSplitOptions.None);
                arr.ToList().ForEach((x) => {
                    sb_.AppendLine(x.Trim());
                });
                return sb_;
            }

            var blockSb = new StringBuilder();
            var logBlock = new LogBlock();
            BlockType? currentBlockType = null;

            while (!reader.EndOfStream)
            {
                var blockType = LogBlock.GetBlockType(reader.PeekLine());

                if (currentBlockType == null && blockType != null)
                {
                    var headerLine = reader.ReadLine();
                    setHeader((BlockType)blockType, blockSb, headerLine);
                    currentBlockType = blockType;
                }
                else if (currentBlockType == null && blockType == null)
                {
                    //just append so we don't miss any logs
                    blockSb.AppendLine(reader.ReadLine());
                }
                else if (currentBlockType != null && blockType == null)
                {
                    //still current block, keep going
                    blockSb.AppendLine(reader.ReadLine());
                }
                else if (currentBlockType != null && blockType != null) {
                    //new block break;
                    break;
                }
                
            }

            //end of current block, or end of last block
            logBlock.BlockType = currentBlockType??BlockType.NA; //in case no block type is found.
            logBlock.Block = FormatBlock(blockSb);
            return logBlock;
        }
        public List<LogBlock> FilterFile(string filePath, List<BlockType> blockTypes)
        {
            var blockList = new List<LogBlock>();
            try
            {
                var fs = new System.IO.FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader reader = new StreamReader(fs))
                {
                    var adapter = new PeekableReaderAdapter(reader);
                    while (!adapter.EndOfStream)
                    {
                        blockList.Add(GetNewBlock(adapter));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorManager.Write(ex);
            }

            blockList.Reverse();
            return blockList.ToList();
            
        }
    }
}
