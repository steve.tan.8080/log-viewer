﻿using ScintillaNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogViewerApp
{
	internal class SearchManager
	{

		public static ScintillaNET.Scintilla TextArea;

		public static string LastSearch = "";

		
		public static int LastSearchPosition = -1;
		public static string LastSearchText = null;

		public static void FindFromCursor(string searchText) 
		{
			if (searchText == null) { return; }
			
			TextArea.TargetStart = TextArea.CurrentPosition;
			TextArea.TargetEnd = TextArea.TextLength;
			TextArea.SearchFlags = SearchFlags.None;

			
			// Search, and if not found..
			if (TextArea.SearchInTarget(searchText) == -1) {
				throw new Exception("seach text not found");
			}

			TextArea.SetSelection(TextArea.TargetEnd, TextArea.TargetStart);
			TextArea.ScrollCaret();

		}

		/*public static void Find(string searchText, bool next, bool incremental)
		{
			bool first = LastSearch != searchText;

			LastSearch = searchText;
			if (LastSearch.Length > 0)
			{

				if (next)
				{

					// SEARCH FOR THE NEXT OCCURANCE

					// Search the document at the last search index
					TextArea.TargetStart = LastSearchIndex - 1;
					TextArea.TargetEnd = LastSearchIndex + (LastSearch.Length + 1);
					TextArea.SearchFlags = SearchFlags.None;

					// Search, and if not found..
					if (!incremental || TextArea.SearchInTarget(LastSearch) == -1)
					{

						// Search the document from the caret onwards
						TextArea.TargetStart = TextArea.CurrentPosition;
						TextArea.TargetEnd = TextArea.TextLength;
						TextArea.SearchFlags = SearchFlags.None;

						// Search, and if not found..
						if (TextArea.SearchInTarget(LastSearch) == -1)
						{

							// Search again from top
							TextArea.TargetStart = 0;
							TextArea.TargetEnd = TextArea.TextLength;

							// Search, and if not found..
							if (TextArea.SearchInTarget(LastSearch) == -1)
							{

								// clear selection and exit
								TextArea.ClearSelections();
								return;
							}
						}

					}

				}
				else
				{

					// SEARCH FOR THE PREVIOUS OCCURANCE

					// Search the document from the beginning to the caret
					TextArea.TargetStart = 0;
					TextArea.TargetEnd = TextArea.CurrentPosition;
					TextArea.SearchFlags = SearchFlags.None;

					// Search, and if not found..
					if (TextArea.SearchInTarget(LastSearch) == -1)
					{

						// Search again from the caret onwards
						TextArea.TargetStart = TextArea.CurrentPosition;
						TextArea.TargetEnd = TextArea.TextLength;

						// Search, and if not found..
						if (TextArea.SearchInTarget(LastSearch) == -1)
						{

							// clear selection and exit
							TextArea.ClearSelections();
							return;
						}
					}

				}

				// Select the occurance
				LastSearchIndex = TextArea.TargetStart;
				TextArea.SetSelection(TextArea.TargetEnd, TextArea.TargetStart);
				TextArea.ScrollCaret();

			}
		}*/

	}
}
